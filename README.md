# Genesis

Genesis is a virtual assistant desined to increase your productivity in the bash shell.

1. Features of Genesis:

* Custom "Wake word" support
* Execute shell commands on-the-fly
* Custom reply voice

2. How to obtain Genesis:

* Using [github](https://github.com/CodeLongAndProsper90/Genesis)
* 2. Using the Genesis installer (WIP)
